#from graphviz import Digraph

class Noeud:
    """
    Un noeud a une valeur et pointe vers deux autres noeuds (petit et
    grand) ou éventuellement le vide.
    On insère de nouvelles valeurs en partant de la racine du noeud et
    en bifurquant selon la comparaison avec la valeur du noeud.

    """
#deja fait
    def __init__(self, val):
        """
        Un noeud a toujours une valeur mais pointe vers un autre noeud
        ou éventuellement le vide (None)
        """
        self.__val = val
        self.__grand = None
        self.__petit = None

#fait
    def insere(self, val):
        if val < self.__val:
        if self.__grand is None:
            self.__grand = Node(val)
            self.__grand.parent = self
        else:
            self.__grand.insert(val)
        elif val > self.__val:
            if self.__petit is None:
                self.__petit = Node(val)
                self.__petit.parent = self
            else:
                self.__petit.insert(val)

  #fait          
    def hauteur(self) -> int:
        return 1 + max(
            self.left.get_height() 
            if self.left 
            else -1, 
            self.right.get_height()
            if self.right 
            else -1)
    
#fait 
    def nb_noeuds(self) -> int:
        n = 0
        while self.__petit and self.__grand is not None:
            n = n+1
            return n

#fait    
    def est_feuille(self) -> bool:
        if self.__petit or self.__grand - 1 is not None:
            return True
        else:
            return False

#fait
    def nb_feuilles(self) -> int:
        """
        J'utilise bool ici car les booléens en python sont un sous type des entiers
        """
        return bool(self.__petit) + bool(self.__grand)
     

#fait
    def visite_pre(self) -> None:
        """
        Que fait ce code ?
        Expliquez la différence avec les deux suivants
        """
        print(self.__val)
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_pre()

    #Ce code va donner le chemin postfixe fixe d'un arbre binaire
    #Le code visite_inf lui le chemin infixe et visite_post le chemin infixe

    def visite_post(self) -> None:
        for cote in [self.__petit, self.__grand]:
            if cote is not None:
                cote.visite_post()
        print(self.__val)

    def visite_inf(self) -> None:
        if self.__petit is not None:
            self.__petit.visite_inf()
        print(self.__val)
        if self.__grand is not None:
            self.__grand.visite_inf()

from collection import deque
#Je n'ai pas reussi a faire ce programme avec la classe noeud donc je l'ai fait avec les listes python

def visite_niveau(arbre, debut):
    visiter = []
    queue = deque()
    queue.append(debut)
    while queue:
        noeud = queue.poppetit()
        if noeud not in visiter:
            visiter.append(noeud)
            non_visiter = [n for n in arbre[noeud] if n not in visiter]
            queue.extend(non_visiter)
    return visiter

#fait
    def mini(self):
        noeud = self
        while noeud.__grand:
            noeud = noeud.__grand
        return noeud
        
#fait
    def maxi(self):
        noeud = self
        while noeud.__petit:
            noeud = noeud.__petit
        return noeud


#Ajout + :

#Verification d'equilibre de l'arbre
def verif_equilibre(self):
    petit = self.__petit._check_balance() if self.__petit else -1
    grand = self.__grand._check_balance() if self.grand else -1
    if abs(left - right) > 1:
        raise ValueError('Unbalanced tree.')
    return max(left, right) + 1

def is_balanced(self):
    try:
        self.verif_equilibre()
        return True
    except ValueError:
        return False
        
    #  Outils de représentation
    
    def viz(self):
        """Renvoie un objet graphviz pour la visualisation graphique de l'arbre"""
        def representation(dot, noeud, aretes):
            if noeud is not None:
                dot.node(str(id(noeud)), str(noeud.valeur))
                # Appel récursif de la fonction representation
                if noeud.__petit is not None:
                    representation(dot, noeud.__petit, aretes)
                    aretes.append((str(id(noeud)) , str(id(noeud.__petit))))
                if noeud.__grand is not None:
                    representation(dot, noeud.__grand, aretes)
                    aretes.append((str(id(noeud)) , str(id(noeud.__grand))))
                    
        dot = Digraph(comment="Arbre binaire", format='svg')
        aretes = []
        representation(dot, self.racine, aretes)
        dot.edges(aretes)
        return dot

    def affiche(self):
        """
        """
        s = self.viz()
        s.graph_attr['ordering']='out'
        return s







#from nbr import *

class Arbre:
    """
    Arbre binaire de recherche constitué de noeuds.
    Reprend les méthodes de la classe Noeud en incluant le cas vide
    et en construisant un arbre à partir d'un noeud.
    """

    def __init__(self) -> None:
        """
        Constructeur : un arbre est vide ou constitué de noeuds
        """
        self.__data = None

    def est_vide(self) -> bool:
        """
        Testeur : vérifie si un arbre est vide
        """
        return self.__data is None

    def insere(self, val) -> None:
        """
        Insère un élément comparable dans un arbre selon le critère
        choisi pour les noeuds.
        Si l'arbre est vide, crée le noeud-data
        """
        if self.__data is None:
            self.__data = Noeud(val)
        else:
            self.__data.insere(val)

    def hauteur(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.hauteur()

    def nb_noeuds(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_noeuds()

    def est_feuille(self) -> bool:
        if self.__data is None:
            return False
        else:
            return self.__data.est_feuille()

    def nb_feuilles(self) -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_feuilles()

    def contient(self, v) -> bool:
        if self.__data is None:
            return False
        return self.__data.contient(v)

    def visite_pre(self) -> None:
        if self.__data:
            self.__data.visite_pre()

    def visite_post(self) -> None:
        if self.__data:
            self.__data.visite_post()

    def visite_inf(self) -> None:
        if self.__data:
            self.__data.visite_inf()

    def visite_inf_imp(self) -> None:
        if self.__data:
            self.__data.visite_inf_imp()

    def visite_niveau(self) -> None:
        if self.__data:
            self.__data.visite_niveau()

    def mini(self):
        assert self.__data, 'Arbre vide ! Pas de minimum'
        return self.__data.mini()

    def maxi(self):
        assert self.__data, 'Arbre vide ! Pas de maximum'
        return self.__data.maxi()

    def affiche(self) -> None:
        assert self.__data, 'Arbre vide'
        return self.__data.affiche()

    def __str__(self) -> str:
        if self.__data is None:
            return 'Arbre_Vide'
        else:
            return self.__data.__str__()



import random

A = Arbre()

for k in random.sample(range(100,800), 35):
    A.insere(k)
    
A.affiche()
